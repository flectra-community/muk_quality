# Flectra Community / muk_quality

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[muk_quality_docs_hr](muk_quality_docs_hr/) | 1.0.1.0.3| Human Resources Extension
[muk_quality_docs_dms](muk_quality_docs_dms/) | 1.0.1.0.5| Quality Management System DMS Support
[muk_quality_docs](muk_quality_docs/) | 1.0.1.1.15| Quality Management System


